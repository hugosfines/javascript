var resultado;
document.getElementById("search-filter").addEventListener('click', function ()
{
	var key = document.getElementById('filter-by').value
	var valor = document.getElementById('text-filter').value
	var value = valor.replace(/\b[a-z]/g,c=>c.toUpperCase());
    filtrarPor(resultado, key, value);
}); 

function listado() {
	var list = '';
	const url = 'https://www.balldontlie.io/api/v1/players'
	const http = new XMLHttpRequest()

	http.open("GET", url)
	http.onreadystatechange = function() {

	    if(this.readyState == 4 && this.status == 200) {
	        resultado = JSON.parse(this.responseText)
	        //console.log(resultado)
	        creaHTML(resultado);
	    }
	}
	http.send()
}

function filtrarPor(array, key, value) {
	if(value=='') {
		creaHTML(resultado);
		return;
	}
	var arr = array.data;
 	var obj = arr.filter(function(e) {
    	return e[key] == value;
 	});
 	var filtro = JSON.parse(JSON.stringify(obj))
 	creaHTMLFiltro(filtro)
}

function creaHTML(resultado) {
    var list = '<tr><td colspan="8"><h3>Lista de Jugadores</h3></td></tr><tr><th>ID</th><th>Nombre</th><th>Apellido</th><th>Equipo</th><th>Ciudad</th><th>Posición</th><th>Estatura</th><th>Acción</th></tr>';
    for(var i=0; i<resultado.data.length; i++) {
    	list += '<tr><td>'+resultado.data[i].id+'</td>' +
    	'<td>'+resultado.data[i].first_name+'</td>' +
    	'<td>'+resultado.data[i].last_name+'</td>' +
    	'<td>'+resultado.data[i].team.full_name+'</td>' +
    	'<td>'+resultado.data[i].team.city+'</td>' +
    	'<td>'+resultado.data[i].position+'</td>' +
    	'<td>'+(resultado.data[i].height_feet!== null ? resultado.data[i].height_feet+' pies' : 'Sin Información') + '</td>' +
    	'<td><button type="button" onclick="borrarJugador('+resultado.data[i].id+')">Borrar jugador</button></td>' +
    	'</tr>';
    }
    document.getElementById('list-players').innerHTML = list;
}

function creaHTMLFiltro(resultado) {
	var list = '<tr><td colspan="7"><h3>Lista de Jugadores</h3></td></tr><tr><th>ID</th><th>Nombre</th><th>Apellido</th><th>Equipo</th><th>Ciudad</th><th>Posición</th><th>Estatura</th></tr>';
    for(var i=0; i<resultado.length; i++) {
    	list += '<tr><td>'+resultado[i].id+'</td>' +
    	'<td>'+resultado[i].first_name+'</td>' +
    	'<td>'+resultado[i].last_name+'</td>' +
    	'<td>'+resultado[i].team.full_name+'</td>' +
    	'<td>'+resultado[i].team.city+'</td>' +
    	'<td>'+resultado[i].position+'</td>' +
    	'<td>'+(resultado[i].height_feet!== null ? resultado[i].height_feet+' pies' : 'Sin Información') + '</td>' +
    	'</tr>';
    }
    document.getElementById('list-players').innerHTML = list;
}

function addNewPlayer() {
	if(document.getElementById('first_name').value != '') {
		var id_player = resultado.data.length + 1;
		var id_team = Math.floor(Math.random() * 100) + 1;
		resultado.data.push({
			id: 			id_player,
		    first_name: 	document.getElementById('first_name').value,
		    last_name: 		document.getElementById('last_name').value,
		    position: 		document.getElementById('position').value,
		    height_feet: 	document.getElementById('height_feet').value,
		    team: 			{
		    	id: 		id_team,
		    	full_name: 	document.getElementById('full_name').value,
		    	city: 		document.getElementById('city').value,
		    } , 
		});
		creaHTML(resultado);
		alert('Registro del jugador ha sigo agregado con exito');
		return
	}
	alert('Indique el nombre del jugador');
}

function cambiarValor() {
	var id = document.getElementById('id-player').value
	var campo = document.getElementById('campo-modif').value
	var nvo_valor = document.getElementById('nvo-valor').value
	if(id!='' || Number(id)>0) {
	 	resultado.data.forEach(function(elemento) {
	 		if(elemento.id == id) {
		 		if(campo=='full_name') {
		 			elemento.team[campo] = nvo_valor
		 		}else{
	 				elemento[campo] = nvo_valor
	 			}
	   		}
		})
		creaHTML(resultado);
	}
}

function buscarJugador() {
	var id = document.getElementById('id-search').value
	var list = '<tr><td colspan="7"><h3>Lista de Jugadores</h3></td></tr><tr><th>ID</th><th>Nombre</th><th>Apellido</th><th>Equipo</th><th>Ciudad</th><th>Posición</th><th>Estatura</th></tr>';

	if(id!='' || Number(id)>0) {
	 	resultado.data.forEach(function(elemento) {
	 		if(elemento.id == id) {
	        	list += '<tr><td>'+elemento.id+'</td>' +
	        	'<td>'+elemento.first_name+'</td>' +
	        	'<td>'+elemento.last_name+'</td>' +
	        	'<td>'+elemento.team.full_name+'</td>' +
	        	'<td>'+elemento.team.city+'</td>' +
	        	'<td>'+elemento.position+'</td>' +
	        	'<td>'+(elemento.height_feet!== null ? elemento.height_feet+' pies' : 'Sin Información') + '</td>' +
	        	'</tr>';
	   		}
		})
		document.getElementById('list-players').innerHTML = list;
	}else{
		creaHTML(resultado);
	}
}

function borrarJugador(id) {
	resultado.data.forEach(function(elemento, key){
	  if(elemento.id==id) 
	  	resultado.data.splice(key,1); 
	});
	creaHTML(resultado);
}